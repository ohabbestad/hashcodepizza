public class PossibleSlice {
    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    private int rows;
    private int columns;

    public PossibleSlice(int rows, int columns){
        this.rows = rows;
        this.columns = columns;
    }
}
