public class Slice {
    private int rowStart;
    private int rowEnd;
    private int columnStart;
    private int columnEnd;
    private Cell[][] cells;
    private Pizza pizza;

    public Slice(int rowStart, int rowEnd, int columnStart, int columnEnd, Pizza pizza){
        this.rowStart = rowStart;
        this.rowEnd = rowEnd;
        this.columnStart = columnStart;
        this.columnEnd = columnEnd;
        this.pizza = pizza;

        this.cells = new Cell[Math.abs(rowEnd-rowStart)+1][Math.abs(columnEnd-columnStart)+1];
        System.out.println("New slice:");
        for(int i = 0; i <= Math.abs(rowEnd-rowStart); i++) {
            for (int j = 0; j <= Math.abs(columnEnd - columnStart); j++) {
                this.cells[i][j] = pizza.getCell(rowStart + i, columnStart + j);
                System.out.print(this.cells[i][j].getIngredient());
            }
            System.out.print("\n");
        }
    }

    public boolean hasMushroomAndTomato(){
        boolean hasMushroom = false;
        boolean hasTomato = false;
        for(int i = 0; i < Math.abs(rowEnd-rowStart); i++){
            for(int j = 0; j < Math.abs(columnEnd-columnStart); j++){
                if (cells[i][j].getIngredient() == 'T')
                    hasMushroom = true;
                else if (cells[i][j].getIngredient() == 'T')
                    hasTomato = true;
            }
        }
        return hasMushroom && hasTomato;
    }

    @Override
    public String toString() {
        String outputText = rowStart + " " + columnStart + " " + rowEnd + " " + columnEnd + "\n";
        return outputText;
    }
}
