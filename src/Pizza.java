import java.util.ArrayList;

public class Pizza {


    private int rows;
    private int columns;
    private Cell[][] cellArray;
    private boolean pizzaBuilt = false;
    private int minNumOfingredients;
    private int maxCellsInSlice;
    private ArrayList<Slice> slices = new ArrayList<>();
    private ArrayList<PossibleSlice> possibleSlices;

    public Pizza(int rows, int columns, int minNumOfingredients, int maxCellsInSlice){
        this.rows = rows;
        this.columns = columns;
        this.cellArray = new Cell[rows][columns];
        this.minNumOfingredients = minNumOfingredients;
        this.maxCellsInSlice = maxCellsInSlice;
        possibleSlices = new ArrayList<>();
        for(int i = 1; i<= maxCellsInSlice; i++)
            if (maxCellsInSlice % i == 0 && (maxCellsInSlice / i) <= this.columns && i<= this.rows) {
                PossibleSlice possibleSlice = new PossibleSlice(i, maxCellsInSlice / i);
                System.out.println("(" + possibleSlice.getRows() + "," + possibleSlice.getColumns() + ")");
                possibleSlices.add(possibleSlice);
            }
    }

    public void buildPizza(ArrayList<String> pizzaData){
        int counter = 0;
        for (int i = 0; i<rows; i++){
            String line = pizzaData.get(counter);
            counter++;
            for(int j = 0; j < columns; j++){
                Cell cell = new Cell(i,j);
                cell.setIngredient(line.charAt(j));
                cellArray[i][j] = cell;
            }
        }
        pizzaBuilt = true;
        System.out.println(this);
    }

    public void addSlice(Slice slice){
        this.slices.add(slice);
    }

    public int getNumberOfSlices(){
        return slices.size();
    }

    public Slice getSlice(int num){
        return slices.get(num);
    }

    public void removeLastSlice(){
        this.slices.remove(this.slices.size()-1);
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public Cell[][] getCells() {
        return cellArray;
    }

    public Cell getCell(int row, int column){
        return cellArray[row][column];
    }

    public boolean isPizzaBuilt() {
        return pizzaBuilt;
    }

    public int getMinNumOfingredients() {
        return minNumOfingredients;
    }

    public int getMaxCellsInSlice() {
        return maxCellsInSlice;
    }

    public ArrayList<Slice> getSlices() {
        return slices;
    }

    @Override
    public String toString() {
        String outputText = "Pizza has " + rows + " rows and " + columns + " columns.\n";
        if(pizzaBuilt) {
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    outputText += cellArray[i][j].getIngredient();
                }
                outputText+= "\n";
            }
        }
        else {
            outputText+= "Pizza has not been built.";
        }
        return outputText;
    }
}
