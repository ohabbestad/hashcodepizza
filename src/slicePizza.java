import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class slicePizza {

    public static void main(String[] args) {
        String filename = "example";

        /**
         * Les fil
         */
        ArrayList<String> pizzaData = read("src/in/" + filename + ".in");

        // Finn "regler" for pizzaen
        String settings = pizzaData.get(0);

        // Ta bort "regeldata, nå er kun selve pizzaen i pizzaData
        pizzaData.remove(0);

        /**
         * Lagre "regler" i egne int-variabler
         */
        int[] listOfSettings = new int[4];
        int counter = 0;
        for (String s : settings.split(" ")){
            listOfSettings[counter] = Integer.parseInt(s);
            counter++;
        }
        int rows = listOfSettings[0];
        int columns = listOfSettings[1];
        int minNumOfingredients = listOfSettings[2];
        int maxCellsInSlice = listOfSettings[3];

        /**
         * Prøvekode
         */
        Pizza pizza = new Pizza(rows, columns, minNumOfingredients, maxCellsInSlice);
        pizza.buildPizza(pizzaData);
        Slice slice1 = new Slice(0,2,0,1, pizza);
        Slice slice2 = new Slice(0,2,2,2, pizza);
        Slice slice3 = new Slice(0,2,3,4, pizza);

        if(slice1.hasMushroomAndTomato()) {
            pizza.addSlice(slice1);
        }

        if (slice2.hasMushroomAndTomato()){
            pizza.addSlice(slice2);
        }

        if (slice3.hasMushroomAndTomato()) {
            pizza.addSlice(slice3);
        }

        pizza.addSlice(slice1);
        pizza.addSlice(slice2);
        pizza.addSlice(slice3);

        write(pizza, filename);


        /**
         * ---------------------------------
         */
    }

    private static ArrayList<String> read(String inputFile) {
        ArrayList<String> pizzaData = new ArrayList<String>();
        File file = new File(inputFile);
        try{
            Scanner in = new Scanner(file);
            while(in.hasNextLine()) {
                String line = in.nextLine();
                pizzaData.add(line);
            }
            in.close();
        }
        // catch (FileNotFoundException e){
        //    System.out.println("File not found:\n" + e.getMessage());
        //}
        catch (IOException e){
            System.out.println("Something went wrong when reading the file:\n" + e);
        }
        return pizzaData;
    }
    public static void write(Pizza pizza, String filename){
        try {
            Writer wr = new FileWriter("out/" + filename + ".out");
            wr.write(String.valueOf(pizza.getNumberOfSlices()));
            wr.write("\n");
            for (int i = 0; i < pizza.getNumberOfSlices(); i++) {
                wr.write(pizza.getSlice(i).toString());
            }

            wr.flush();
            wr.close();
        }
        catch (IOException e){
            System.out.println("IOException\n" + e);
        }
    }
}
