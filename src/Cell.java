public class Cell {
    private int row;
    private int column;
    private char ingredient;
    private boolean isSlice;

    public Cell(int row, int column){
        this.row = row;
        this.column = column;
        isSlice = false;
    }

    public boolean hasMushroom(){
        return this.ingredient == 'M';
    }

    public boolean hasTomato(){
        return this.ingredient == 'T';
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public char getIngredient() {
        return ingredient;
    }

    public void setIngredient(char ingredient) {
        this.ingredient = ingredient;
    }

    public boolean isSlice() {
        return isSlice;
    }

    public void setSlice(boolean slice) {
        isSlice = slice;
    }
}
